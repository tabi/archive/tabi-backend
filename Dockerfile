FROM golang:1.10.4-alpine AS builder

ENV GO_PROJECT_PATH gitlab.com/tabi/tabi-backend

WORKDIR /go/src/$GO_PROJECT_PATH

RUN apk --no-cache add git jq curl build-base

RUN curl -o /usr/local/bin/swagger -L'#' $(curl -s https://api.github.com/repos/go-swagger/go-swagger/releases/latest | jq -r '.assets[] | select(.name | contains("'"$(uname | tr '[:upper:]' '[:lower:]')"'_amd64")) | .browser_download_url')
RUN chmod +x /usr/local/bin/swagger

RUN go get github.com/rakyll/statik

COPY . .

RUN statik -f -src=public -dest .
RUN swagger generate spec -o public/swagger.json -b ./cmd/tabi-backend
RUN statik -f -src=public -dest .

RUN go install ./...

FROM alpine:3.5

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates

WORKDIR /usr/local/bin

COPY --from=builder /go/bin/tabi-backend .
COPY --from=builder /go/src/gitlab.com/tabi/tabi-backend/docker-server.toml server.toml

EXPOSE 8000

CMD ["./tabi-backend"]
