package responses

import (
	"gitlab.com/tabi/tabi-backend/api/messages"
)

// TokenMessageResponse
// swagger:response TokenMessageResponse
type TokenMessageResponse struct {
	// in: body
	Body messages.TokenResult
}

// A UserMessage model.
//
// This is used for operations that want an Order as body of the request
// swagger:parameters loginUser
type LoginUserParameters struct {
	// in: body
	Body messages.UserLoginMessage
}

// A UserMessage model.
//
// This is used for operations that want an Order as body of the request
// swagger:parameters registerUser
type RegisterUserParameters struct {
	// in: body
	Body messages.UserMessage
}
