package responses

import (
	"gitlab.com/tabi/tabi-backend/model"
)

// swagger:parameters addDevice
type AddDeviceParameters struct {
	// in: path
	Uid int `json:"uid"`

	// in: body
	// required: true
	Body model.Device
}

// ListDeviceResponse returns all devices belonging to a user
// swagger:response listDevices
type ListDeviceResponse struct {
	// in: body
	Body []model.Device
}

// GetDeviceResponse returns requested device
// swagger:response getDevice
type GetDeviceResponse struct {
	// in: body
	Body model.Device
}

// swagger:parameters getDevice
type GetDeviceParameters struct {
	// in: path
	Uid int `json:"uid"`

	// in: path
	Did int `json:"did"`
}

// swagger:parameters getDevices
type GetDevicesParameters struct {
	// in: path
	Uid int `json:"uid"`
}
