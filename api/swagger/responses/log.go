package responses

import "gitlab.com/tabi/tabi-backend/model"

// swagger:parameters addLogs
type AddLogsParameters struct {
	// User identifier
	// in: path
	Uid int `json:"uid"`

	// Device identifier
	// in: path
	Did int `json:"did"`

	// Attribute value
	// in: body
	Body []model.Log
}

// swagger:parameters getLogs
type GetLogsParameters struct {
	// User identifier
	// in: path
	Uid int `json:"uid"`

	// Device identifier
	// in: path
	Did int `json:"did"`
}
