package v1

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
	"strconv"

	"time"
)

// PostBatteryInfo retrieves the data being sent
func PostBatteryInfo(s *server.Server) httprouter.Handle {
	// swagger:operation POST /user/{uid}/device/{did}/battery battery addBattery
	// ---
	// summary: Add battery data
	// produces:
	// - application/json
	// responses:
	//   "200":
	//     "$ref": "#/responses/empty"
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did, err := strconv.Atoi(ps.ByName("did"))
		if err != nil {
			log.WithField("error", err).Error("Could not get did from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		uid, err := strconv.Atoi(ps.ByName("uid"))
		if err != nil {
			log.WithField("error", err).Error("Could not get uid from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		device, err := s.Store.GetDevice(uint(did))

		if !deviceBelongsToUser(uid, device) {
			http.Error(w, "device does not match user", 400)
			log.Error("Empty request body")
		}

		var batteryInfos []*model.BatteryInfo
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		err = json.NewDecoder(r.Body).Decode(&batteryInfos)
		if err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json batteryinfo")
			return
		}

		for _, v := range batteryInfos {
			v.DeviceId = device.ID
		}

		s.Store.CreateBatteryInfos(batteryInfos)

	}
}

func GetBatteryInfo(s *server.Server) httprouter.Handle {
	// swagger:operation GET /user/{uid}/device/{did}/battery battery getBattery
	// ---
	// summary: Get battery data of a device
	// produces:
	// - application/json
	// responses:
	//   "200":
	//     "$ref": "#/responses/getBattery"
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did, err := strconv.Atoi(ps.ByName("did"))
		if err != nil {
			log.WithField("error", err).Error("Could not get did from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		batteryInfos, err := s.Store.GetBatteryInfos(uint(did), time.Time{}, time.Now())

		if err != nil {
			http.Error(w, "server error", 500)
			log.WithField("error", err).Error("GetBatteryInfo: store error")

		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(batteryInfos)

	}
}
