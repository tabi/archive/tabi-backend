package v1

import (
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
	"strconv"
)

func GetConfig(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

		uid, err := strconv.Atoi(ps.ByName("uid"))
		if err != nil {
			log.WithField("error", err).Error("Could not get uid from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		userConfig, err := s.Store.GetConfigByUserId(uint(uid))
		if err != nil {
			log.WithField("error", err).Error("Could not get config")
			http.Error(w, "Could not get config", 400)
			return
		}

		fmt.Println(userConfig)

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(userConfig.Config)
	}
}
