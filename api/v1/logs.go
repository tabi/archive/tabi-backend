package v1

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
	"strconv"
	"time"
)

func PostLogs(s *server.Server) httprouter.Handle {
	// swagger:operation POST /user/{uid}/device/{did}/logs/ logs addLogs
	// ---
	// summary: Post logs
	// produces:
	// - application/json
	// responses:
	//   "200":
	//     "$ref": "#/responses/empty"
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did, err := strconv.Atoi(ps.ByName("did"))
		if err != nil {
			log.WithField("error", err).Error("Could not get did from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		uid, err := strconv.Atoi(ps.ByName("uid"))
		if err != nil {
			log.WithField("error", err).Error("Could not get uid from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		device, err := s.Store.GetDevice(uint(did))

		if !deviceBelongsToUser(uid, device) {
			//todo  error
		}

		var logs []*model.Log
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}
		err = json.NewDecoder(r.Body).Decode(&logs)
		if err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json logs")
			return
		}

		for _, l := range logs {
			l.DeviceId = device.ID
			l.UserId = uint(uid)
		}

		s.Store.CreateLogs(logs)
	}
}

func GetLogs(s *server.Server) httprouter.Handle {
	// swagger:operation GET /user/{uid}/device/{did}/logs/ logs getLogs
	// ---
	// summary: Get logs
	// produces:
	// - application/json
	// responses:
	//   "200":
	//     "$ref": "#/responses/empty"
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did, err := strconv.Atoi(ps.ByName("did"))
		if err != nil {
			log.WithField("error", err).Error("Could not get did from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		uid, err := strconv.Atoi(ps.ByName("uid"))
		if err != nil {
			log.WithField("error", err).Error("Could not get uid from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		device, err := s.Store.GetDevice(uint(did))

		if !deviceBelongsToUser(uid, device) {
			//todo  error
		}

		logs, err := s.Store.GetLogs(uint(did), time.Time{}, time.Now())

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(logs)

	}
}
