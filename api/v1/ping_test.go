package v1

import (
	"encoding/json"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPing(t *testing.T) {
	request, _ := http.NewRequest("GET", "/create", nil)
	response := httptest.NewRecorder()
	GetPing(&server.Server{})(response, request, nil)
	checkResponseCode(t, http.StatusOK, response.Code)

	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)
	if m["Available"] != true {
		t.Errorf("Expected the boolean to be true. Got %v", m["Available"])
	}
}
