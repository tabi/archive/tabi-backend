package v1

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
	"strconv"
)

// PostBatteryInfo retrieves the data being sent
func PostQuestions(s *server.Server) httprouter.Handle {
	// swagger:operation POST /user/{uid}/device/{did}/question question addQuestion
	// ---
	// summary: Add question data
	// produces:
	// - application/json
	// responses:
	//   "200":
	//     "$ref": "#/responses/empty"
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did, err := strconv.Atoi(ps.ByName("did"))
		if err != nil {
			log.WithField("error", err).Error("Could not get did from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		device, err := s.Store.GetDevice(uint(did))

		var questions []*model.Question
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		err = json.NewDecoder(r.Body).Decode(&questions)
		if err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json batteryinfo")
			return
		}

		for _, v := range questions {
			v.DeviceId = device.ID
		}

		s.Store.CreateQuestions(questions)

	}
}
