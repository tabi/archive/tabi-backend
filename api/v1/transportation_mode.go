package v1

import (
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"

	"encoding/json"
	"gitlab.com/tabi/tabi-backend/model/null"
)

func PostTransportationModes(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		device, err := CheckUserDeviceQueryParameters(s.Store, ps)

		if err != nil {
			http.Error(w, "device could not be verified with user", 400)
			log.WithField("error", err).Error("device could not be verified with user")
		}

		var transportationModes []*model.TransportationMode

		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		err = json.NewDecoder(r.Body).Decode(&transportationModes)
		if err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json transportationmode")
			return
		}

		for _, value := range transportationModes {
			localTrackId := uint(value.LocalTrackId.Int64)
			track, err := s.Store.GetTrackByDeviceAndLocalId(device.ID, localTrackId)

			if err == nil {
				value.TrackId = null.ToNullInt64(int64(track.ID))
			} else {
				value.TrackId.Valid = false
			}
			value.DeviceId = null.ToNullInt64(int64(device.ID))
		}

		if err = s.Store.CreateTransportationModes(transportationModes); err != nil {
			log.WithField("error", err).Error("could not save transportationmode")
			http.Error(w, "could not save transportationmode", 400)
		}
	}

}
