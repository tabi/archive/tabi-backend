package v1

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"

	"errors"
	"gitlab.com/tabi/tabi-backend/store"
	"strconv"
)

// deviceBelongsToUser returns true if device belongs to user
func deviceBelongsToUser(userId int, device *model.Device) bool {
	return device.UserId == uint(userId)
}

func convertUserDeviceStrings(userIdStr string, deviceIdStr string) (userId uint, deviceId uint, err error) {
	did, err := strconv.Atoi(deviceIdStr)
	if err != nil {
		return 0, 0, errors.New("Could not convert deviceIdStr")
	}

	uid, err := strconv.Atoi(userIdStr)
	if err != nil {
		return 0, 0, errors.New("Could not convert userIdStr")
	}

	return uint(uid), uint(did), nil
}

func CheckUserDeviceQueryParameters(store store.Store, params httprouter.Params) (*model.Device, error) {
	uid, did, err := convertUserDeviceStrings(params.ByName("uid"), params.ByName("did"))

	if err != nil {
		return nil, err
	}

	device, err := store.GetDevice(did)
	if err != nil {
		return nil, errors.New("Could retrieve device id")
	}

	if !deviceBelongsToUser(int(uid), device) {
		return nil, errors.New("Device does not belong to this user")
	}

	return device, nil
}

func PostPositionEntries(s *server.Server) httprouter.Handle {
	// swagger:operation POST /user/{uid}/device/{did}/positionentry positions addPositions
	// ---
	// summary: Add positions to device
	// produces:
	// - application/json
	// responses:
	//   "200":
	//     "$ref": "#/responses/empty"
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		log.Debug("Post Position Entries")
		did, err := strconv.Atoi(ps.ByName("did"))
		if err != nil {
			log.WithField("error", err).Error("Could not get did from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		uid, err := strconv.Atoi(ps.ByName("uid"))
		if err != nil {
			log.WithField("error", err).Error("Could not get uid from parameters")
			http.Error(w, "request invalid", 400)
			return
		}

		device, err := s.Store.GetDevice(uint(did))

		if err != nil {
			http.Error(w, "device error", 400)
		}

		if !deviceBelongsToUser(uid, device) {

		}

		var pos []*model.PositionEntry
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			return
		}
		err = json.NewDecoder(r.Body).Decode(&pos)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		for _, p := range pos {
			p.DeviceID = uint(did)
		}

		err = s.Store.CreatePositions(pos)

		if err != nil {
			http.Error(w, "store error", 400)
			return
		}
	}

}

func GetUserReport(s *server.Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		events, err := s.Store.GetTokenGeneratedUniqueEvents()

		if err != nil {
			http.Error(w, "failure to get report", 500)
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(events)
	}

}
