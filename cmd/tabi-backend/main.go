package main

//go:generate statik -src=../../public -dest ../..

import (
	"github.com/kardianos/service"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/tabi/tabi-backend/router"
	"gitlab.com/tabi/tabi-backend/server"
	"os"
	"runtime"
)

var logger service.Logger

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

type program struct {
	server     *server.Server
	configPath string
}

func (p *program) Start(s service.Service) error {
	// Start should not block. Do the actual work async.
	go p.run()
	return nil
}
func (p *program) run() {
	log.Info("Starting API Server")

	s, httpserver := server.NewServer(p.configPath)
	p.server = s

	r := router.Load(s)
	log.WithFields(log.Fields{"host": s.Conf.Host}).Info("Listening")
	httpserver.Handler = r
	if httpserver.TLSConfig != nil {
		log.Fatal(httpserver.ListenAndServeTLS("", ""))
	} else {
		log.Fatal(httpserver.ListenAndServe())

	}
}
func (p *program) Stop(s service.Service) error {
	// Stop should not block. Return with a few seconds.
	p.server.Close()
	return nil
}

func CreateService(configPath string) service.Service {
	svcConfig := &service.Config{
		Name:        "TabiBackendApiService",
		DisplayName: "Tabi backend api",
		Description: "Tabi backend providing api services.",
		Arguments:   []string{"--config", configPath},
	}

	prg := &program{configPath: configPath}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		log.Fatal(err)
	}

	return s
}

func main() {
	var config string
	var svcFlag string

	app := cli.NewApp()

	app.Version = "0.0.1"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "config, c",
			Usage:       "toml configuration file",
			Value:       "server.toml",
			Destination: &config,
		},
		cli.StringFlag{
			Name:        "service",
			Usage:       "service flag",
			Destination: &svcFlag,
		},
	}

	app.Action = func(c *cli.Context) {
		RunService(config, svcFlag)
	}

	app.Run(os.Args)
}

func RunService(configPath string, svcFlag string) {
	s := CreateService(configPath)
	logger, err := s.Logger(nil)
	if err != nil {
		log.Fatal(err)
	}

	if len(svcFlag) != 0 {
		err := service.Control(s, svcFlag)
		if err != nil {
			log.Printf("Valid actions: %q\n", service.ControlAction)
			log.Fatal(err)
		}
		return
	}

	err = s.Run()
	if err != nil {
		logger.Error(err)
	}
}
