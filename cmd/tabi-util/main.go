package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/tabi/tabi-backend/server"
	"io/ioutil"
	"os"
)

func main() {
	app := cli.NewApp()

	app.Version = "0.0.1"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config, c",
			Usage: "toml configuration file",
			Value: "server.toml"},
	}

	app.Commands = []cli.Command{
		{
			Name:    "client",
			Aliases: []string{"t"},
			Usage:   "add an api client",
			Subcommands: []cli.Command{
				{
					Name:  "add",
					Usage: "add a new client",
					Action: func(c *cli.Context) error {
						_, store := server.SetupConfigAndStore(c.GlobalString("config"))

						logrus.SetOutput(ioutil.Discard)

						clientId := c.Args().Get(0)
						clientKey := c.Args().Get(1)

						if clientId == "" {
							fmt.Println("Should provide id and key. Command: client add Api-Client123 privatekey1234secret")
						}
						err := addClient(store, clientId, clientKey)
						if err != nil {
							fmt.Printf("Could not store client into store: %s\n", err)
						} else {
							fmt.Println("Added client to store")
						}
						return nil
					},
				},
				{
					Name:  "remove",
					Usage: "remove an existing client",
					Action: func(c *cli.Context) error {
						_, store := server.SetupConfigAndStore(c.GlobalString("config"))

						removeClient(store, c.Args().First())

						return nil
					},
				},
			},
		},
		{
			Name:    "user",
			Aliases: []string{"t"},
			Usage:   "add a user",
			Subcommands: []cli.Command{
				{
					Name:  "add",
					Usage: "add a new client",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "admin, a",
							Usage: "admin user"},
						cli.StringFlag{
							Name:  "username, u",
							Usage: "username"},
						cli.StringFlag{
							Name:  "password, p",
							Usage: "password"},
						cli.StringFlag{
							Name:  "first-name",
							Usage: "first name"},
						cli.StringFlag{
							Name:  "last-name",
							Usage: "last name"},
					},
					Action: CreateUserCommand,
				},
			},
		},
		{
			Name:  "user-config-csv",
			Usage: "load configs from csv",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "csvfile",
					Usage: "path to csv file"},
			},
			Action: SetupUserDeviceConfigCsvCommand,
		},
	}

	app.Run(os.Args)
}
