package config

type Config struct {
	Host                 string
	SecretKey            string
	DbType               DbType
	Postgres             PostgresConfig    `toml:"postgres"`
	Sqlite               SqliteConfig      `toml:"sqlite"`
	LetsEncrypt          LetsEncryptConfig `toml:"letsencrypt"`
	Redis                RedisConfig       `toml:"redis"`
	Metrics              MetricsConfig     `tom:"metrics"`
	LogLevel             LogLevel
	OpenRegistration     bool
	ClientAuthentication bool
}

// LetsEncryptConfig contains the information needed to enable LetsEncrypt
type LetsEncryptConfig struct {
	Enabled   bool
	Domain    string
	AcceptTOS bool
}

// SqliteConfig contains configuration for a sqlite database connection
type SqliteConfig struct {
	Source string
}

// PostgresConfig contains the configuration for a postgres database connection
type PostgresConfig struct {
	Host     string
	Port     int
	User     string
	Database string
	SslMode  bool
	Password string
}

type MetricsConfig struct {
	Username string
	Password string
}

type RedisConfig struct {
	Host string
	Port int
}

type LogLevel string

const (
	Debug LogLevel = "debug"
	Info           = "info"
	Warn           = "warn"
	Error          = "error"
)

type DbType string

const (
	Postgres DbType = "postgres"
	Sqlite          = "sqlite"
)
