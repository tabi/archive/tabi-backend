package config

import "github.com/BurntSushi/toml"

func LoadTomlFile(file string) (config Config, err error) {
	_, err = toml.DecodeFile(file, &config)
	return
}
