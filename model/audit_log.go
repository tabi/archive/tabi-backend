package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
)

type EventLog struct {
	Id        int64
	Username  null.NullString
	UserId    null.NullInt64 `db:"user_id"`
	Event     null.NullString
	Message   null.NullString
	Path      null.NullString
	Result    null.NullString
	IpAddress null.NullString `db:"ip_address"`
	ClientId  null.NullString `db:"client_id"`
	Timestamp null.NullTime
}

type Event string

const (
	TokenGenerated   Event = "TokenGenerated"
	UserRegistration Event = "UserRegistration"
	UnknownClient    Event = "UnknownClient"
)
