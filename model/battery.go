package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
	"time"
)

// BatteriesInfo represents a list of batteryinfo
//
//
// swagger:model
type BatteriesInfo []BatteryInfo

type BatteryInfo struct {
	Id           uint
	DeviceId     uint `db:"device_id"`
	BatteryLevel int  `db:"battery_level"`
	State        uint
	Timestamp    time.Time
	CreatedAt    null.NullTime `db:"created_at" json:",omitempty"`
	UpdatedAt    null.NullTime `db:"updated_at" json:",omitempty"`
	DeletedAt    null.NullTime `db:"deleted_at" json:",omitempty"`
}

type BatteryState uint

const (
	Discharging BatteryState = 0
	Charging                 = 1
	Full                     = 2
	NotCharging              = 3
	Unknown                  = 4
)
