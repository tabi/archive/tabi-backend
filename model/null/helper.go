package null

import (
	"database/sql"
	"time"
)

func ToNullString(str string) NullString {
	return NullString{NullString: sql.NullString{String: str, Valid: str != ""}}
}

func ToNullTime(time time.Time) NullTime {
	return NullTime{Time: time, Valid: true}
}

func ToNullInt64(i int64) NullInt64 {
	return NullInt64{NullInt64: sql.NullInt64{Int64: i, Valid: true}}
}
