package model

import "gitlab.com/tabi/tabi-backend/model/null"

type Question struct {
	DeviceId     uint
	Identifier   string
	Answer       string
	QuestionDate null.NullTime
	Timestamp    null.NullTime
}
