package model

import (
	"gitlab.com/tabi/tabi-backend/model/null"
	"time"
)

type SensorMeasurementSession struct {
	ID            int64
	DeviceId      uint           `db:"device_id"`
	AmbientLight  null.NullInt64 `db:"ambient_light"`
	Pedometer     null.NullInt64
	Proximity     null.NullBool
	Compass       null.NullInt64
	BatteryLevel  null.NullInt64 `db:"battery_level"`
	BatteryStatus BatteryState   `db:"battery_status"`
	Timestamp     time.Time
	CreatedAt     null.NullTime `db:"created_at"`
	UpdatedAt     null.NullTime `db:"updated_at"`
	DeletedAt     null.NullTime `db:"deleted_at"`
}
