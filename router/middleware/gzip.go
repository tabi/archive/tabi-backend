package middleware

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

type gzipResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

func (w gzipResponseWriter) Write(b []byte) (int, error) {
	if w.ResponseWriter.Header().Get("Content-Type") == "" {
		contentType := http.DetectContentType(b)
		w.ResponseWriter.Header().Set("Content-Type", contentType)
	}

	return w.Writer.Write(b)
}

func GzipMiddlewareFunc(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {
	// Decode body if content is encoded in gzip
	if r.Header.Get("Content-Encoding") == "gzip" {
		gzipReader, err := gzip.NewReader(r.Body)
		if err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(rw, "request with Content-Encoding: gzip failed to decode")
		}
		logrus.Debug("Request has Content Encoding is gzip")

		body, _ := ioutil.ReadAll(gzipReader)
		r.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	}

	// If client accepts gzip encoding send a gzip responsewriter
	if strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
		rw.Header().Set("Content-Encoding", "gzip")
		gz := gzip.NewWriter(rw)
		defer gz.Close()
		gzr := gzipResponseWriter{Writer: gz, ResponseWriter: rw}

		next(gzr, r, ps)
	} else {
		next(rw, r, ps)
	}
}
