package router

import (
	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rakyll/statik/fs"
	"gitlab.com/tabi/tabi-backend/router/middleware"
	"gitlab.com/tabi/tabi-backend/server"
	"gitlab.com/vpl/httproutermiddleware"
	"net/http"

	"fmt"
	"gitlab.com/tabi/tabi-backend/api/v1"
	_ "gitlab.com/tabi/tabi-backend/statik"
)

const (
	apiV1 string = "/api/v1"
)

func Index(system http.FileSystem) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		http.ServeFile(w, r, r.URL.Path[1:])
	}
}

func Load(s *server.Server) http.Handler {
	router := httprouter.New()

	statikFS, err := fs.New()
	if err != nil {
		panic(err)
	}

	fs := http.FileServer(statikFS)
	router.Handler("GET", "/swagger-ui/*e", fs)

	router.Handler("GET", "/swagger.json", fs)

	simpleMetrics := middleware.NewBasicAuthenticationMiddleware(s.Conf.Metrics.Username, s.Conf.Metrics.Password)
	metricsMiddleware := httproutermiddleware.New(simpleMetrics)
	wrappedPromHandler := httproutermiddleware.WrapIntoHttpRouter(promhttp.Handler())
	router.Handle("GET", "/metrics", metricsMiddleware.UseHandleAndServe(wrappedPromHandler))

	v1.RegisterRoutes(s, router)

	return router
}

func newPrefixFileSystem(prefix string, fs http.FileSystem) http.FileSystem {
	return &prefixFileSystem{fs, prefix}
}

type prefixFileSystem struct {
	fs     http.FileSystem
	prefix string
}

func (p *prefixFileSystem) Open(name string) (http.File, error) {
	fmt.Println(name)
	fmt.Println(p.prefix + name)

	file, err := p.fs.Open(p.prefix + name)

	return file, err
}
