package sql

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

func TestShouldCreateBatteryInfos(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	batteries := []*model.BatteryInfo{
		{DeviceId: 1,
			BatteryLevel: 50,
			State:        uint(model.Discharging),
			Timestamp:    time.Now(),
			CreatedAt:    null.ToNullTime(time.Now())}}

	mock.ExpectBegin()
	mock.ExpectPrepare("COPY").
		ExpectExec().
		WithArgs(1, 50, 0, AnyTime{}, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = ds.CreateBatteryInfos(batteries)

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestShouldGetBatteryInfos(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	columns := []string{"id", "device_id", "battery_level", "state", "timestamp", "created_at", "updated_at", "deleted_at"}

	mock.ExpectQuery("SELECT (.+) FROM battery_infos").
		WillReturnRows(sqlmock.NewRows(columns).
			AddRow(21, 1, 10, 1, time.Now(), time.Now(), time.Time{}, time.Time{}).
			AddRow(23, 1, 100, 1, time.Now(), time.Now(), time.Time{}, time.Time{}))

	actualBatteries, err := ds.GetBatteryInfos(1, time.Time{}, time.Now())

	if len(actualBatteries) != 2 {
		t.Errorf("expected %d. actual %d.", 2, len(actualBatteries))
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}
