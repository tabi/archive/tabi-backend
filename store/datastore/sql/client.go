package sql

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

var createClientQuery = `INSERT INTO clients (id, key, allowed, created_at)
VALUES ($1, $2, $3, $4)`

func (ds *datastore) CreateClient(client *model.Client) error {
	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("client create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("client create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("client create commit")
			return
		}
	}()

	tx.MustExec(createClientQuery, client.Id, client.Key, client.Allowed, time.Now())

	return err
}

var updateClientQuery = `UPDATE clients SET (key, allowed, modified_at) =
 ($2, $3, $4) WHERE id = $1`

func (ds *datastore) UpdateClient(client *model.Client) error {
	if client.Key == "" {
		return errors.New("empty key")
	}

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("client update tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("client update rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("client update commit")
			return
		}
	}()

	res, err := tx.Exec(updateClientQuery, client.Id, client.Key, client.Allowed, time.Now())

	if err != nil {
		log.WithField("error", err).Error("client update exec")
		return err
	}
	rows, err := res.RowsAffected()

	if err != nil {
		log.WithField("error", err).Error("client update rows affected")
		return err
	}

	if rows != int64(1) {
		log.Error("client create update more rows")
		return errors.New("more rows update than should be")
	}

	return err
}

func (ds *datastore) GetClient(id string) (*model.Client, error) {
	if id == "" {
		return nil, errors.New("empty id/pk")
	}

	var client model.Client

	err := ds.DB.Get(&client, "SELECT * FROM clients WHERE id = $1 AND deleted_at IS NULL", id)

	if err != nil {
		log.WithField("error", err).Error("user get")
		return nil, err
	}

	return &client, nil
}
