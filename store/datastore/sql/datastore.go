package sql

import (
	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/store/datastore/sql/migrations"
)

type datastore struct {
	*sqlx.DB
}

func NewDatastore(db *sqlx.DB) *datastore {
	ds := &datastore{db}
	ds.PerformMigrations()

	return ds
}

func (ds *datastore) PerformMigrations() error {
	migrationsMap := migrations.ReturnMigrations()
	mgs, _ := ds.ListMigrations()

	migrationsMap.IterateOverMigrations(func(key string, value string) {
		alreadyIn := ds.checkMigrationInList(mgs, key)

		if alreadyIn {
			log.Debug(key + " was already migrated")
		} else {
			log.Info(key + " is migrating")

			err := ds.migrate(value)
			if err != nil {
				log.WithField("error", err).Fatal("Could not migrate")
				panic("Could not migrate")
			}
			ds.storeSuccesfulMigration(key)
		}
	})

	return nil
}

func (ds *datastore) storeSuccesfulMigration(key string) error {
	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("Failure to store successful migration")
		return err
	}

	_, err = tx.Exec("INSERT INTO migrations VALUES ($1)", key)

	if err != nil {
		log.WithField("error", err).Error("Failure to store successful migration")
		return err
	}

	err = tx.Commit()

	if err != nil {
		log.WithField("error", err).Error("Failure to store successful migration")
		return err
	}

	return nil
}

func (ds *datastore) migrate(statement string) error {
	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("Failure to migrate")
		return err
	}

	_, err = tx.Exec(statement)

	if err != nil {
		log.WithField("error", err).Error("Failure to migrate")
		return err
	}

	err = tx.Commit()

	if err != nil {
		log.WithField("error", err).Error("Failure to commit migrate")
		return err
	}

	return nil
}

func (ds *datastore) checkMigrationInList(migrations []string, migration string) bool {
	for _, element := range migrations {
		if element == migration {
			return true
		}
	}
	return false
}

func (ds *datastore) ListMigrations() ([]string, error) {
	rows, err := ds.DB.Queryx("SELECT * FROM migrations")
	if err != nil {
		log.WithField("error", err).Error("Failure to list migrations")
		return nil, err
	}

	var migrations []string

	for rows.Next() {
		var migrationName string
		err = rows.Scan(&migrationName)
		if err != nil {
			log.WithField("error", err).Error("Failure to scan into migrationname")
			return migrations, err
		}

		migrations = append(migrations, migrationName)
	}

	return migrations, nil
}

func (ds *datastore) Close() {
	ds.DB.Close()
}
