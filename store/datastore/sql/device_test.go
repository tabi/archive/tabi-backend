package sql

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

func TestShouldCreateDevice(t *testing.T) {
	//setup mock datastore and database
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	//arrange
	device := model.Device{UserId: 1,
		Manufacturer:    null.ToNullString("Samsung"),
		Model:           null.ToNullString("Galaxy S6 Edge"),
		OperatingSystem: null.ToNullString("Android")}
	var lastInsertId uint = 32

	//act
	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO devices").
		WithArgs(device.UserId, device.Manufacturer, device.Model, device.OperatingSystem, device.OperatingSystemVersion, AnyTime{}).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(lastInsertId))
	mock.ExpectCommit()

	err = ds.CreateDevice(&device)

	//assert
	if device.ID != lastInsertId {
		t.Errorf("pk not returned in device properly")
	}

	if err != nil {
		t.Errorf("Error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Unfulfilled expectations: %s", err)
	}
}

func TestShouldUpdateDevice(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	//arrange
	device := model.Device{ID: 3,
		UserId:          0,
		Manufacturer:    null.ToNullString("Samsung"),
		Model:           null.ToNullString("Galaxy S7 Edge"),
		OperatingSystem: null.ToNullString("Android")}
	var lastInsertId uint = 3

	//act
	mock.ExpectBegin()
	mock.ExpectExec("UPDATE devices").
		WithArgs(AnyTime{}, device.DeletedAt, device.UserId, device.Manufacturer, device.Model, device.OperatingSystem, device.ID).
		WillReturnResult(sqlmock.NewResult(int64(lastInsertId), 1))
	mock.ExpectCommit()

	err = ds.UpdateDevice(&device)

	//assert
	if err != nil {
		t.Errorf("Erorr '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Unfulfilled expectations: %s", err)
	}
}

func TestFailUpdateDeviceOnEmptyId(t *testing.T) {
	db, _, _ := createMockSqlxDb()
	ds := createDatastore(db)

	expectedError := "empty id/pk"
	device := model.Device{UpdatedAt: null.ToNullTime(time.Now()),
		UserId:          1,
		Manufacturer:    null.ToNullString("Manufacturer"),
		Model:           null.ToNullString("SomeModel"),
		OperatingSystem: null.ToNullString("Android")}

	actualErr := ds.UpdateDevice(&device)

	if actualErr == nil {
		t.Errorf("%s was expected. No error occurred", expectedError)
	}

	if actualErr.Error() != expectedError {
		t.Errorf("unexpected error. expected: %s actual %s", expectedError, actualErr)
	}
}

func TestShouldGetDevice(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	//arrange
	deviceId := 3
	expectedDevice := model.Device{ID: 42,
		UserId:          1,
		Manufacturer:    null.ToNullString("Apple"),
		Model:           null.ToNullString("iPhone 7"),
		OperatingSystem: null.ToNullString("iOS")}

	columns := []string{"id", "user_id", "manufacturer", "model", "os", "os_version", "created_at", "updated_at", "deleted_at"}

	mock.ExpectQuery("SELECT (.+) FROM devices").
		WithArgs(deviceId).
		WillReturnRows(sqlmock.NewRows(columns).
			AddRow("42", "1", "Apple", "iPhone 7", "iOS", "11.3", time.Now(), time.Time{}, time.Time{}))

	//act
	actualDevice, err := ds.GetDevice(3)

	//assert
	if actualDevice.Model != expectedDevice.Model {
		t.Errorf("expected %s. actual %s.", expectedDevice.Model.String, actualDevice.Model.String)
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestShouldGetAllDevices(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	columns := []string{"id", "user_id", "manufacturer", "model", "os", "os_version", "created_at", "updated_at", "deleted_at"}

	mock.ExpectQuery("SELECT (.+) FROM devices").
		WillReturnRows(sqlmock.NewRows(columns).
			AddRow("42", "1", "Apple", "iPhone 7", "iOS", "11.3", time.Now(), time.Time{}, time.Time{}).
			AddRow("43", "1", "Apple", "iPhone 8", "iOS", "11.3", time.Now(), time.Time{}, time.Time{}))

	actualDevices, err := ds.GetDevices()

	if len(actualDevices) != 2 {
		t.Errorf("expected %d. actual %d.", 2, len(actualDevices))
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}
