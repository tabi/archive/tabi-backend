package sql

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

func TestShouldCreateGravity(t *testing.T) {
	//setup mock datastore and database
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	//arrange
	gravity := model.Gravity{MotionSensor: &model.MotionSensor{X: 0.513122559, Y: 4.95935059, Z: 7.82096863, Timestamp: time.Now(), DeviceId: 1}}
	var lastInsertId uint64 = 32

	//act
	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO gravities").
		WithArgs(gravity.X, gravity.Y, gravity.Z, gravity.Timestamp, gravity.DeviceId).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(lastInsertId))
	mock.ExpectCommit()

	err = ds.CreateGravity(&gravity)

	//assert
	if gravity.ID != lastInsertId {
		t.Errorf("pk not returned in user properly")
	}

	if err != nil {
		t.Errorf("Erorr '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Unfulfilled expectations: %s", err)
	}
}

func TestShouldCreateGravitiest(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	gravities := []*model.Gravity{{&model.MotionSensor{X: 0.513122559, Y: 4.95935059, Z: 7.82096863, Timestamp: time.Now(), DeviceId: 1}}}
	//var lastInsertId uint = 32

	mock.ExpectBegin()
	mock.ExpectPrepare("COPY").
		ExpectExec().
		WithArgs(1, 0.513122559, 4.95935059, 7.82096863, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = ds.CreateGravities(gravities)

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestShouldGetGravities(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	columns := []string{"id", "device_id", "x", "y", "z", "timestamp"}

	mock.ExpectQuery("SELECT (.+) FROM gravities").
		WillReturnRows(sqlmock.NewRows(columns).
			AddRow(1, 15, 0.513122559, 4.95935059, 7.82096863, time.Now()).
			AddRow(2, 15, 0.513122559, 4.95935059, 7.82096863, time.Now()))

	actualgravities, err := ds.GetGravities(1, time.Time{}, time.Now())

	if len(actualgravities) != 2 {
		t.Errorf("expected %d. actual %d.", 2, len(actualgravities))
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}
