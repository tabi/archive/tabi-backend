package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateLog(logEntry *model.Log) error {
	err := ds.CreateLogs([]*model.Log{logEntry})

	return err
}

func (ds *datastore) CreateLogs(logs []*model.Log) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("logs create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("logs create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("logs create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("logs", "device_id", "origin", "event", "message", "timestamp", "created_at"))

	for _, v := range logs {
		_, err = stmt.Exec(v.DeviceId, v.Origin, v.Event, v.Message, v.Timestamp, time.Now())

		if err != nil {
			log.WithField("error", err).Error("logs create exec")
			return err
		}
	}

	err = stmt.Close()
	if err != nil {
		log.WithField("error", err).Error("logs create stmt close")
	}

	return err
}

func (ds *datastore) GetLogs(deviceId uint, startTime time.Time, endTime time.Time) (logs []*model.Log, err error) {

	err = ds.DB.Select(&logs, "SELECT * FROM logs WHERE device_id = $1 AND (timestamp BETWEEN $2 AND $3)", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("logs get")
		return nil, err
	}

	return logs, err
}
