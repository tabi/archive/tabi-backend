package sql

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"

	//"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

func TestShouldCreateLogs(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	logs := []*model.Log{{DeviceId: 1,
		Origin:    null.ToNullString("error"),
		Event:     null.ToNullString("Event"),
		Message:   null.ToNullString("message"),
		Timestamp: null.ToNullTime(time.Now()),
		CreatedAt: null.ToNullTime(time.Now())}}
	//var lastInsertId uint = 32

	mock.ExpectBegin()
	mock.ExpectPrepare("COPY").
		ExpectExec().
		WithArgs(1, "error", "Event", "message", AnyTime{}, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = ds.CreateLogs(logs)

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestShouldGetLogs(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	columns := []string{"id", "origin", "event", "message", "user_id", "device_id", "timestamp", "created_at", "updated_at", "deleted_at"}

	mock.ExpectQuery("SELECT (.+) FROM logs").
		WillReturnRows(sqlmock.NewRows(columns).
			AddRow("21", "origin1", "event1", "message1", "1", "1", time.Now(), time.Now(), time.Time{}, time.Time{}).
			AddRow("22", "origin2", "event2", "message2", "1", "1", time.Now(), time.Now(), time.Time{}, time.Time{}))

	actualLogs, err := ds.GetLogs(1, time.Time{}, time.Now())

	if len(actualLogs) != 2 {
		t.Errorf("expected %d. actual %d.", 2, len(actualLogs))
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}
