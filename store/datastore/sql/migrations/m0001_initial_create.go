package migrations

var m0001initialCreate = `CREATE TABLE migrations (
id text NOT NULL PRIMARY KEY
);

CREATE TABLE users (
id serial NOT NULL PRIMARY KEY,
username text NOT NULL,
first_name text,
last_name text,
password text,
email text,
role integer,
disabled boolean,
created_at timestamp with time zone,
modified_at timestamp with time zone,
deleted_at timestamp with time zone
);

CREATE TABLE devices (
id serial NOT NULL PRIMARY KEY,
user_id integer REFERENCES users(id) ON DELETE CASCADE,
manufacturer text,
model text,
os text,
os_version text,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);

CREATE TABLE accelerometers (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
x numeric,
y numeric,
z numeric,
"timestamp" timestamp with time zone
);

CREATE TABLE attributes (
device_id integer NOT NULL REFERENCES devices(id) ON DELETE CASCADE,
key text NOT NULL,
value text,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone,
PRIMARY KEY(device_id, key)
);

CREATE TABLE battery_infos (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
battery_level integer,
state integer,
"timestamp" timestamp with time zone,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);

CREATE TABLE gravities (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
x numeric,
y numeric,
z numeric,
"timestamp" timestamp with time zone
);


CREATE TABLE gyroscopes (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
x numeric,
y numeric,
z numeric,
"timestamp" timestamp with time zone
);


CREATE TABLE linear_accelerations (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
x numeric,
y numeric,
z numeric,
"timestamp" timestamp with time zone
);


CREATE TABLE logs (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
origin text,
event text,
message text,
"timestamp" timestamp with time zone,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);


CREATE TABLE magnetometers (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
x numeric,
y numeric,
z numeric,
"timestamp" timestamp with time zone
);


CREATE TABLE orientations (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
x numeric,
y numeric,
z numeric,
"timestamp" timestamp with time zone
);

CREATE TABLE position_entries (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
latitude numeric,
longitude numeric,
accuracy numeric,
speed numeric,
altitude numeric,
heading numeric,
desired_accuracy numeric,
distance_between_previous_position numeric,
"timestamp" timestamp with time zone,
created_at timestamp with time zone
);

CREATE TABLE quaternions (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
x numeric,
y numeric,
z numeric,
"timestamp" timestamp with time zone,
w numeric
);

CREATE TABLE sensor_measurement_sessions (
id bigserial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
ambient_light integer,
pedometer integer,
proximity boolean,
battery_level integer,
battery_status integer,
"timestamp" timestamp with time zone,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone);

CREATE TABLE tracks (
id serial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
local_id integer,
start_time timestamp with time zone,
end_time timestamp with time zone,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);

CREATE TABLE user_stops (
id serial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
local_id integer,
name text,
latitude numeric,
longitude numeric,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);

CREATE TABLE user_stop_visits (
id serial NOT NULL PRIMARY KEY,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
local_id integer,
stop_id integer REFERENCES user_stops(id) ON DELETE CASCADE,
local_stop_id integer,
begin_timestamp timestamp with time zone,
end_timestamp timestamp with time zone,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);

CREATE TABLE transportation_modes (
id serial NOT NULL PRIMARY KEY,
track_id integer REFERENCES tracks(id) ON DELETE CASCADE,
device_id integer REFERENCES devices(id) ON DELETE CASCADE,
local_track_id integer,
walk bool,
run bool,
mobility_scooter bool,
car bool,
bike bool,
moped bool,
scooter bool,
motorcycle bool,
train bool,
subway bool,
tram bool,
bus bool,
other bool,
"timestamp" timestamp with time zone,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);`
