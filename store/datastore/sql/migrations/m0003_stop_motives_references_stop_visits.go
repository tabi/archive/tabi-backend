package migrations

var m0003stopMotivesReferencesStopVisits = `
ALTER TABLE user_stop_motives
DROP CONSTRAINT user_stop_motives_stop_visit_id_fkey,
ADD CONSTRAINT user_stop_motives_stop_visit_id_fkey FOREIGN KEY (stop_visit_id) REFERENCES user_stop_visits(id);
`
