package migrations

var m0006addEventLogsIndexes = `
CREATE TABLE event_logs (
id bigserial NOT NULL PRIMARY KEY,
user_id integer REFERENCES users(id) ON DELETE CASCADE,
event text,
message text,
path text,
result text,
client_id text,
ip_address text,
"timestamp" timestamp with time zone
);

CREATE INDEX idx_audit_logs_timestamp ON event_logs (timestamp);

CREATE UNIQUE INDEX idx_users_username ON users (username);
`
