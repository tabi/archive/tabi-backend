package migrations

var m0008addConfig = `
CREATE TABLE user_device_config (
id bigserial NOT NULL PRIMARY KEY,
user_id integer REFERENCES users(id) ON DELETE CASCADE,
config jsonb,
created_at timestamp with time zone,
updated_at timestamp with time zone,
deleted_at timestamp with time zone
);

CREATE UNIQUE INDEX idx_user_device_config_user_id ON user_device_config (user_id);
`
