package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreatePosition(position *model.PositionEntry) error {
	err := ds.CreatePositions([]*model.PositionEntry{position})

	return err
}

func (ds *datastore) CreatePositions(positions []*model.PositionEntry) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("position create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("position create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("position create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(
		pq.CopyIn("position_entries",
			"device_id",
			"latitude",
			"longitude",
			"accuracy",
			"speed",
			"altitude",
			"heading",
			"desired_accuracy",
			"distance_between_previous_position",
			"timestamp",
			"created_at"))

	for _, v := range positions {
		_, err = stmt.Exec(v.DeviceID, v.Latitude, v.Longitude,
			v.Accuracy, v.Speed, v.Altitude,
			v.Heading, v.DesiredAccuracy,
			v.DistanceBetweenPreviousPosition, v.Timestamp, time.Now())

		if err != nil {
			log.WithField("error", err).Error("position create exec")
			return err
		}
	}
	err = stmt.Close()
	if err != nil {
		log.WithField("error", err).Error("position create stmt close")
		return err
	}

	return err
}

func (ds *datastore) GetPositions(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.PositionEntry, error) {
	var positions []*model.PositionEntry

	err := ds.DB.Select(&positions, "SELECT * FROM position_entries WHERE device_id = ? AND timestamp >= ? AND timestamp =< ?", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("logs get")
		return nil, err
	}

	return positions, err
}
