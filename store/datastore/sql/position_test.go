package sql

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

func TestShouldCreatePositions(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	positions := []*model.PositionEntry{{DeviceID: 1,
		Latitude: 2.323, Longitude: 1.232, Accuracy: 20,
		Speed: 11, Altitude: 100, Heading: 3, DesiredAccuracy: 20,
		DistanceBetweenPreviousPosition: 3, Timestamp: time.Now(), CreatedAt: time.Now()}}

	mock.ExpectBegin()
	mock.ExpectPrepare("COPY").
		ExpectExec().
		WithArgs(1, 2.323, 1.232, 20.0, 11.0, 100.0, 3.0, 20.0, 3.0, AnyTime{}, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = ds.CreatePositions(positions)

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}
