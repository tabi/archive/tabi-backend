package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateQuaternion(quaternion *model.Quaternion) error {
	var insertQuery = `INSERT INTO quaternions ("x", "y", "z", "timestamp", "device_id")
	VALUES ($1, $2, $3, $4, $5)`

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("quaternion create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("quaternion create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("quaternion create commit")
			return
		}
	}()

	row := tx.QueryRow(insertQuery, quaternion.X, quaternion.Y, quaternion.Z, quaternion.Timestamp, quaternion.DeviceId)

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("quaternion create queryrow")
		return err
	}

	quaternion.ID = uint64(id)

	return err
}

func (ds *datastore) CreateQuaternions(quaternions []*model.Quaternion) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("quaternion create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("quaternion create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("quaternion create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("quaternions", "device_id", "x", "y", "z", "timestamp"))

	for _, v := range quaternions {
		_, err = stmt.Exec(v.DeviceId, v.X, v.Y, v.Z, v.Timestamp)

		if err != nil {
			log.WithField("error", err).Error("quaternion create exec")
			return err
		}
	}

	err = stmt.Close()

	if err != nil {
		log.WithField("error", err).Error("quaternion could not close statement")
	}

	return err
}

func (ds *datastore) GetQuaternions(deviceId uint, startTime time.Time, endTime time.Time) (quaternions []*model.Quaternion, err error) {
	err = ds.DB.Select(&quaternions, "SELECT * FROM quaternions WHERE device_id = ? AND timestamp >= ? AND timestamp =< ?", deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("quaternions get")
		return nil, err
	}

	return quaternions, err
}
