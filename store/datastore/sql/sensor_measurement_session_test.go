package sql

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

//
//func TestShouldCreateSensorMeasurementSession(t *testing.T) {
//	//setup mock datastore and database
//	db, mock, err := createMockSqlxDb()
//	ds := createDatastore(db)
//
//	//arrange
//	session := model.SensorMeasurementSession{DeviceId: 3, AmbientLight: null.ToNullInt64(3), BatteryStatus: 1, BatteryLevel: null.ToNullInt64(30), Timestamp: time.Now()}
//	var lastInsertId uint64 = 32
//
//	//act
//	mock.ExpectBegin()
//	mock.ExpectQuery("INSERT INTO sensor_measurement_sessions").
//		WithArgs(session.DeviceId, session.AmbientLight, session.Pedometer, session.Proximity, session.BatteryLevel, session.BatteryStatus, AnyTime{}, AnyTime{}).
//		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(lastInsertId))
//	mock.ExpectCommit()
//
//	err = ds.CreateSensorMeasurementSession(&session)
//
//	//assert
//	if session.ID != int64(lastInsertId) {
//		t.Errorf("pk not returned in user properly")
//	}
//
//	if err != nil {
//		t.Errorf("Erorr '%s' was not expected", err)
//	}
//
//	if err := mock.ExpectationsWereMet(); err != nil {
//		t.Errorf("Unfulfilled expectations: %s", err)
//	}
//}

func TestShouldCreateSensorMeasurementSessions(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	session := model.SensorMeasurementSession{DeviceId: 3, AmbientLight: null.ToNullInt64(3), BatteryStatus: 1, BatteryLevel: null.ToNullInt64(30), Timestamp: time.Now()}

	sessions := []*model.SensorMeasurementSession{&session}
	//var lastInsertId uint = 32

	mock.ExpectBegin()
	mock.ExpectPrepare("COPY").
		ExpectExec().
		WithArgs(session.DeviceId, session.AmbientLight, session.Pedometer, session.Proximity, session.BatteryLevel, session.BatteryStatus, AnyTime{}, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = ds.CreateSensorMeasurementSessions(sessions)

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestShouldGetSensorMeasurementSessions(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	columns := []string{"id", "device_id", "ambient_light", "pedometer", "proximity", "battery_level", "battery_status", "timestamp", "created_at", "updated_at", "deleted_at"}

	mock.ExpectQuery("SELECT (.+) FROM sensor_measurement_sessions").
		WillReturnRows(sqlmock.NewRows(columns).
			AddRow(1, 2, 2, 40, true, 20, 1, time.Now(), time.Now(), time.Time{}, time.Time{}).
			AddRow(2, 2, 2, 40, true, 20, 1, time.Now(), time.Now(), time.Time{}, time.Time{}))

	actualSessions, err := ds.GetSensorMeasurementSessions(2, time.Time{}, time.Now())

	if len(actualSessions) != 2 {
		t.Errorf("expected %d. actual %d.", 2, len(actualSessions))
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}
