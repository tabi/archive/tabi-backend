package sql

import (
	"database/sql/driver"
	"github.com/jmoiron/sqlx"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"time"
)

type AnyTime struct{}

// Match satisfies sqlmock.Argument interface
func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

func createMockSqlxDb() (dbx *sqlx.DB, mock sqlmock.Sqlmock, err error) {
	db, mock, err := sqlmock.New()

	dbx = sqlx.NewDb(db, "sqlmock")

	return
}

func createDatastore(db *sqlx.DB) *datastore {
	return &datastore{db}
}
