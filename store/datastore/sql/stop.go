package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"

	"errors"
	"time"
)

func (ds *datastore) CreateStop(stop *model.UserStop) error {
	panic("implement me")
}

func (ds *datastore) CreateStops(stops []*model.UserStop) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("stops create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("stops create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("stops create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("user_stops", "device_id", "local_id", "name", "latitude", "longitude", "created_at"))

	for _, v := range stops {
		_, err = stmt.Exec(v.DeviceId, v.LocalId, v.Name, v.Latitude, v.Longitude, time.Now())

		if err != nil {
			log.WithField("error", err).Error("stops create exec")
			return err
		}
	}

	err = stmt.Close()
	if err != nil {
		log.WithField("error", err).Error("stops create stmt close")
	}

	return err
}

func (ds *datastore) UpdateStop(stop *model.UserStop) error {
	panic("implement me")
}

func (ds *datastore) GetStop(id uint) (*model.UserStop, error) {
	var getQuery = `SELECT *
					FROM user_stops
					WHERE id = $1`

	if id == 0 {
		return nil, errors.New("empty id/pk")
	}

	var stop model.UserStop

	err := ds.Get(&stop, getQuery, id)

	if err != nil {
		log.WithField("error", err).Error("device get")
		return nil, err
	}

	return &stop, nil
}

func (ds *datastore) GetStopByDeviceAndLocalId(deviceId uint, localStopId uint) (*model.UserStop, error) {
	var getQuery = `SELECT *
					FROM user_stops
					WHERE device_id = $1
					AND local_id = $2`

	if deviceId == 0 || localStopId == 0 {
		return nil, errors.New("empty ids")
	}

	var stop model.UserStop

	err := ds.Get(&stop, getQuery, deviceId, localStopId)

	if err != nil {
		log.WithField("error", err).Error("device get")
		return nil, err
	}

	return &stop, nil
}

func (ds *datastore) GetStopsByDevice(deviceId uint) ([]*model.UserStop, error) {
	var stops []*model.UserStop

	err := ds.DB.Select(&stops, "SELECT * FROM user_stops WHERE device_id = ?", deviceId)

	if err != nil {
		log.WithField("error", err).Error("GetStopsByDevice")
		return nil, err
	}

	return stops, err
}
