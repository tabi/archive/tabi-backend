package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateStopMotives(stopMotives []*model.UserStopMotive) error {
	var err error

	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("stopmotive create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("stopmotive create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("stopmotive create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("user_stop_motives", "device_id", "stop_visit_id", "local_stop_visit_id", "motive", "timestamp", "created_at"))

	for _, v := range stopMotives {
		_, err = stmt.Exec(v.DeviceId, v.StopVisitId, v.LocalStopVisitId, v.Motive, v.Timestamp, time.Now())

		if err != nil {
			log.WithField("error", err).Error("stopmotive create exec")
			return err
		}
	}

	err = stmt.Close()
	if err != nil {
		log.WithField("error", err).Error("stopmotive create stmt close")
	}

	return err
}
