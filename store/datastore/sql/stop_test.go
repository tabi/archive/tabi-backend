package sql

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

func TestShouldCreateStops(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	stop := model.UserStop{DeviceId: 1,
		LocalId:   2,
		Name:      null.ToNullString("CBS"),
		Latitude:  1.23,
		Longitude: 1.23}

	stops := []*model.UserStop{&stop}

	mock.ExpectBegin()
	mock.ExpectPrepare("COPY").
		ExpectExec().
		WithArgs(stop.DeviceId, stop.LocalId, stop.Name, stop.Longitude, stop.Latitude, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = ds.CreateStops(stops)

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestShouldGetStopsByDeviceId(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	columns := []string{"id", "device_id", "local_id", "name", "latitude", "longitude", "created_at", "updated_at", "deleted_at"}

	mock.ExpectQuery("SELECT (.+) FROM user_stops").
		WillReturnRows(sqlmock.NewRows(columns).
			AddRow(1, 2, 3, "CBS", 1.23, 1.23, time.Now(), time.Time{}, time.Time{}).
			AddRow(1, 2, 4, "AH", 1.23, 1.23, time.Now(), time.Time{}, time.Time{}))

	actualLogs, err := ds.GetStopsByDevice(2)

	if len(actualLogs) != 2 {
		t.Errorf("expected %d. actual %d.", 2, len(actualLogs))
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}
