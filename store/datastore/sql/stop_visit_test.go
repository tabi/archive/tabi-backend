package sql

import (
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/model/null"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

func TestShouldCreateStopVisits(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	visit := model.UserStopVisit{DeviceId: 1, LocalId: 3, StopId: null.ToNullInt64(4), LocalStopId: 5, BeginTimestamp: time.Now(), EndTimestamp: time.Now()}
	visits := []*model.UserStopVisit{&visit}
	//var lastInsertId uint = 32

	mock.ExpectBegin()
	mock.ExpectPrepare("COPY").
		ExpectExec().
		WithArgs(visit.DeviceId, visit.LocalId, visit.StopId, visit.LocalStopId, AnyTime{}, AnyTime{}, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = ds.CreateStopVisits(visits)

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}

func TestShouldGetStopVisits(t *testing.T) {
	db, mock, err := createMockSqlxDb()
	ds := createDatastore(db)

	columns := []string{"id", "device_id", "local_id", "stop_id", "local_stop_id", "begin_timestamp", "end_timestamp", "created_at", "updated_at", "deleted_at"}

	mock.ExpectQuery("SELECT (.+) FROM user_stop_visits").
		WillReturnRows(sqlmock.NewRows(columns).
			AddRow(1, 1, 3, 3, 3, time.Now(), time.Now(), time.Now(), time.Time{}, time.Time{}).
			AddRow(2, 1, 3, 3, 3, time.Now(), time.Now(), time.Now(), time.Time{}, time.Time{}))

	actualLogs, err := ds.GetStopVisitsByDevice(1, time.Time{}, time.Now())

	if len(actualLogs) != 2 {
		t.Errorf("expected %d. actual %d.", 2, len(actualLogs))
	}

	if err != nil {
		t.Errorf("error '%s' was not expected", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("unfulfilled expectations: %s", err)
	}
}
