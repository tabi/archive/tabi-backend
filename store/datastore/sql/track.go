package sql

import (
	"errors"
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateTrack(track *model.Track) error {
	err := ds.CreateTracks([]*model.Track{track})

	return err
}

func (ds *datastore) CreateTracks(tracks []*model.Track) error {
	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("tracks create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("tracks create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("tracks create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("tracks", "device_id", "local_id", "start_time", "end_time", "created_at"))

	for _, v := range tracks {
		_, err = stmt.Exec(v.DeviceId, v.LocalId, v.StartTime, v.EndTime, time.Now())

		if err != nil {
			log.WithField("error", err).Error("tracks create exec")
			return err
		}
	}

	err = stmt.Close()
	if err != nil {
		log.WithField("error", err).Error("tracks create stmt close")
	}

	return err
}

func (ds *datastore) GetTrack(id uint) (*model.Track, error) {
	var getQuery = `SELECT *
					FROM tracks
					WHERE id = $1`

	if id == 0 {
		return nil, errors.New("empty id/pk")
	}

	var track model.Track

	err := ds.Get(&track, getQuery, id)

	if err != nil {
		log.WithField("error", err).Error("track get")
		return nil, err
	}

	return &track, nil
}

func (ds *datastore) GetTrackByDeviceAndLocalId(deviceId uint, localTrackId uint) (*model.Track, error) {
	var getQuery = `SELECT *
					FROM tracks
					WHERE device_id = $1
					AND local_id = $2`

	if deviceId == 0 || localTrackId == 0 {
		return nil, errors.New("empty ids")
	}

	var track model.Track

	err := ds.Get(&track, getQuery, deviceId, localTrackId)

	if err != nil {
		log.WithField("error", err).Error("device get")
		return nil, err
	}

	return &track, nil
}

func (ds *datastore) GetTracks(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.Track, error) {
	var tracks []*model.Track

	err := ds.DB.Select(&tracks,
		"SELECT * FROM tracks WHERE device_id = ? AND timestamp >= ? AND timestamp =< ?",
		deviceId, startTime, endTime)

	if err != nil {
		log.WithField("error", err).Error("tracks get")
		return nil, err
	}

	return tracks, err
}
