package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

func (ds *datastore) CreateTrackMotives(tracks []*model.TrackMotive) error {
	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("trackmotives create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("trackmotives create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("trackmotives create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(pq.CopyIn("track_motives", "device_id", "track_id", "local_track_id", "motive", "timestamp", "created_at"))

	for _, v := range tracks {
		_, err = stmt.Exec(v.DeviceId, v.TrackId, v.LocalTrackId, v.Motive, v.Timestamp, time.Now())

		if err != nil {
			log.WithField("error", err).Error("trackmotives create exec")
			return err
		}
	}

	err = stmt.Close()
	if err != nil {
		log.WithField("error", err).Error("trackmotives create stmt close")
	}

	return err
}
