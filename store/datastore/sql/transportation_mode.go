package sql

import (
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/model"

	"errors"
	"gitlab.com/tabi/tabi-backend/model/null"
	"time"
)

func (ds *datastore) CreateTransportationMode(mode *model.TransportationMode) error {
	var insertQuery = `INSERT INTO transportation_modes 
	(track_id, device_id, local_track_id, active_modes, timestamp, created_at)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) RETURNING id`

	tx, err := ds.DB.Beginx()
	if err != nil {
		log.WithField("error", err).Error("CreateTransportationMode tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("CreateTransportationMode rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("CreateTransportationMode commit")
			return
		}
	}()

	createdTime := time.Now()

	row := tx.QueryRow(insertQuery,
		mode.TrackId,
		mode.DeviceId,
		mode.LocalTrackId,
		pq.Array(mode.ActiveModes),
		mode.Timestamp,
		createdTime)

	var id int
	err = row.Scan(&id)

	if err != nil {
		log.WithField("error", err).Error("CreateTransportationMode QueryRow")
		return err
	}

	mode.ID = uint(id)
	mode.CreatedAt = null.ToNullTime(createdTime)

	return err
}

func (ds *datastore) CreateTransportationModes(modes []*model.TransportationMode) error {
	tx, err := ds.DB.Beginx()

	if err != nil {
		log.WithField("error", err).Error("CreateTransportationModes create tx begin")
		return err
	}

	defer func() {
		if err != nil {
			log.WithField("error", err).Error("CreateTransportationModes create rollback")
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			log.WithField("error", err).Error("CreateTransportationModes create commit")
			return
		}
	}()

	stmt, err := tx.Preparex(
		pq.CopyIn("transportation_modes",
			"track_id",
			"device_id",
			"local_track_id",
			"active_modes",
			"timestamp",
			"created_at"))

	for _, v := range modes {
		_, err = stmt.Exec(v.TrackId,
			v.DeviceId,
			v.LocalTrackId,
			pq.Array(v.ActiveModes),
			v.Timestamp,
			time.Now())

		if err != nil {
			log.WithField("error", err).Error("CreateTransportationModes create exec")
			return err
		}
	}

	err = stmt.Close()
	if err != nil {
		log.WithField("error", err).Error("CreateTransportationModes create stmt close")
	}

	return err
}

func (ds *datastore) GetTransportationMode(trackId uint) (*model.TransportationMode, error) {
	if trackId == 0 {
		return nil, errors.New("empty id/pk")
	}

	var mode model.TransportationMode

	err := ds.DB.Get(&mode, "SELECT * FROM transportation_modes WHERE id = $1", trackId)

	if err != nil {
		log.WithField("error", err).Error("mode get")
		return nil, err
	}

	return &mode, nil
}
