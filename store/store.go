package store

import (
	"gitlab.com/tabi/tabi-backend/model"
	"time"
)

//go:generate mockgen -destination=../mocks/mock_store.go -package=mocks gitlab.com/tabi/tabi-backend/store Store

type Store interface {
	CreateClient(client *model.Client) error

	UpdateClient(client *model.Client) error

	GetClient(string) (*model.Client, error)

	CreateEvent(event *model.EventLog) error
	GetTokenGeneratedUniqueEvents() ([]*model.EventLog, error)

	// CreateUser creates a new user
	CreateUser(*model.User) error

	// UpdateUser updates a user
	UpdateUser(*model.User) error

	// GetUser returns a user by ID (pk)
	GetUser(uint) (*model.User, error)

	GetUserByUsername(string) (*model.User, error)

	GetUserList() ([]*model.User, error)

	// CreateDevice creates a device
	CreateDevice(*model.Device) error
	UpdateDevice(*model.Device) error
	GetDevice(uint) (*model.Device, error)
	GetDevices() ([]*model.Device, error)
	GetDevicesByUserId(userId uint) ([]*model.Device, error)

	CreateAttribute(*model.Attribute) error
	GetAttribute(deviceId uint, attributeKey string) (*model.Attribute, error)
	GetAttributeList(deviceId uint) ([]*model.Attribute, error)

	CreateLog(log *model.Log) error
	CreateLogs(logs []*model.Log) error
	GetLogs(deviceId uint, startTime time.Time, endTime time.Time) (logs []*model.Log, err error)

	CreateTrack(track *model.Track) error
	CreateTracks(tracks []*model.Track) error
	GetTrack(id uint) (*model.Track, error)
	GetTracks(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.Track, error)
	GetTrackByDeviceAndLocalId(deviceId uint, localTrackId uint) (*model.Track, error)

	CreateTrackMotives(tracks []*model.TrackMotive) error

	CreateStop(stop *model.UserStop) error
	CreateStops(stops []*model.UserStop) error
	UpdateStop(stop *model.UserStop) error
	GetStop(uint) (*model.UserStop, error)
	GetStopByDeviceAndLocalId(deviceId uint, stopId uint) (*model.UserStop, error)
	GetStopsByDevice(deviceId uint) ([]*model.UserStop, error)

	CreateStopMotives(stops []*model.UserStopMotive) error

	CreateStopVisit(visit *model.UserStopVisit) error
	CreateStopVisits(visits []*model.UserStopVisit) error
	GetStopVisit(uint) (*model.UserStopVisit, error)
	GetStopVisitByDeviceAndLocalId(deviceId uint, localId uint) (*model.UserStopVisit, error)
	GetStopVisitsByDevice(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.UserStopVisit, error)

	CreateBatteryInfo(*model.BatteryInfo) error
	CreateBatteryInfos([]*model.BatteryInfo) error
	GetBatteryInfos(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.BatteryInfo, error)

	CreateTransportationMode(mode *model.TransportationMode) error
	CreateTransportationModes(modes []*model.TransportationMode) error
	GetTransportationMode(trackId uint) (*model.TransportationMode, error)

	CreateAccelerometer(*model.Accelerometer) error
	CreateAccelerometers([]*model.Accelerometer) error
	GetAccelerometers(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.Accelerometer, error)

	CreateGyroscope(*model.Gyroscope) error
	CreateGyroscopes([]*model.Gyroscope) error
	GetGyroscopes(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.Gyroscope, error)

	CreateMagnetometer(*model.Magnetometer) error
	CreateMagnetometers([]*model.Magnetometer) error
	GetMagnetometers(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.Magnetometer, error)

	CreateLinearAcceleration(*model.LinearAcceleration) error
	CreateLinearAccelerations([]*model.LinearAcceleration) error
	GetLinearAccelerations(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.LinearAcceleration, error)

	CreateOrientation(*model.Orientation) error
	CreateOrientations([]*model.Orientation) error
	GetOrientations(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.Orientation, error)

	CreateGravity(*model.Gravity) error
	CreateGravities([]*model.Gravity) error
	GetGravities(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.Gravity, error)

	CreateQuaternion(*model.Quaternion) error
	CreateQuaternions([]*model.Quaternion) error
	GetQuaternions(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.Quaternion, error)

	CreateSensorMeasurementSession(*model.SensorMeasurementSession) error
	CreateSensorMeasurementSessions([]*model.SensorMeasurementSession) error
	GetSensorMeasurementSessions(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.SensorMeasurementSession, error)

	CreatePosition(*model.PositionEntry) error
	CreatePositions([]*model.PositionEntry) error
	GetPositions(deviceId uint, startTime time.Time, endTime time.Time) ([]*model.PositionEntry, error)

	CreateQuestions(questions []*model.Question) error

	GetConfigByUserId(userId uint) (*model.UserDeviceConfig, error)
	CreateUserDeviceConfig(config *model.UserDeviceConfig) error

	Close()
}
