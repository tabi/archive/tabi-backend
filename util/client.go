package util

import "net/http"

func GetClientInfoFromRequest(r *http.Request) (string, string) {
	clientId := r.Header.Get("X-Tabi-Client-Identifier")
	clientKey := r.Header.Get("X-Tabi-Client-Key")

	return clientId, clientKey
}
