package httproutermiddleware

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

type mockResponseWriter struct{}

func (m *mockResponseWriter) Header() (h http.Header) {
	return http.Header{}
}

func (m *mockResponseWriter) Write(p []byte) (n int, err error) {
	return len(p), nil
}

func (m *mockResponseWriter) WriteString(s string) (n int, err error) {
	return len(s), nil
}

func (m *mockResponseWriter) WriteHeader(int) {}

func createNewHandler(result string) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		fmt.Fprint(w, result)
	}
}

type MockMiddleware struct {
	Counter int
	Name    string
}

func (m *MockMiddleware) ServeHandle(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {
	m.Counter++
	next(rw, r, ps)
}

func newMiddlewareStruct(name string) *MockMiddleware {
	return &MockMiddleware{Name: name}
}

func newMiddlewareFunc(name string) (HandlerFunc, *MockMiddleware) {
	counter := new(MockMiddleware)

	handler := func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {
		counter.Counter++
		fmt.Println("Printing from middleware " + name)
		next(rw, r, ps)
	}

	return handler, counter
}

func TestMiddlewareHandlerStructBased(t *testing.T) {
	midRoot := New()

	mid1 := newMiddlewareStruct("mid1")
	midRoot.UseHandler(mid1)

	req, _ := http.NewRequest("GET", "/user/gopher", nil)

	w := new(mockResponseWriter)
	ps := httprouter.Params{}

	midRoot.GetHandle()(w, req, ps)

	expected := 1
	value := mid1.Counter

	if expected != value {
		t.Errorf("Expected %d call to middleware instead %d", expected, value)
	}
}

func TestMiddlewareHandlerFuncBased(t *testing.T) {
	midRoot := New()

	mid1, counter1 := newMiddlewareFunc("Name 1")

	midRoot.UseHandler(mid1)

	req, _ := http.NewRequest("GET", "/user/gopher", nil)

	w := new(mockResponseWriter)
	ps := httprouter.Params{}

	midRoot.GetHandle()(w, req, ps)

	expected := 1
	value := counter1.Counter

	if expected != value {
		t.Errorf("Expected %d call to middleware instead %d", expected, value)
	}
}

func TestMiddlewareHandlerCombined(t *testing.T) {
	midRoot := New()

	mid1, counter1 := newMiddlewareFunc("Name 2")
	mid2 := newMiddlewareStruct("mid2")
	mid3, counter3 := newMiddlewareFunc("mid3")
	mid4 := newMiddlewareStruct("mid4")

	midRoot.UseHandler(mid1)
	midRoot.UseHandler(mid2)
	midRoot.UseHandler(mid3)
	midRoot.UseHandler(mid4)

	req, _ := http.NewRequest("GET", "/user/gopher", nil)

	w := new(mockResponseWriter)
	ps := httprouter.Params{}

	midRoot.GetHandle()(w, req, ps)

	testCounter(t, counter1, 1, "mid1")
	testCounter(t, mid2, 1, "mid2")
	testCounter(t, counter3, 1, "mid3")
	testCounter(t, mid4, 1, "mid4")

}

func TestMiddlewareHandlerCombinedWithEndPoint(t *testing.T) {
	midRoot := New()

	handler1 := createNewHandler("Result from handler 1")
	handler2 := createNewHandler("Result from handler 2")

	mid1, counter1 := newMiddlewareFunc("")
	mid2 := newMiddlewareStruct("mid2")
	mid3, counter3 := newMiddlewareFunc("")
	mid4 := newMiddlewareStruct("mid4")

	midRoot.UseHandler(mid1)
	midRoot.UseHandler(mid2)
	midSubOne := midRoot.With(mid3)
	midSubTwo := midRoot.With(mid4)

	requestHandler1 := midSubOne.UseHandleAndServe(handler1)
	_ = midSubTwo.UseHandleAndServe(handler2)

	r, _ := http.NewRequest("GET", "/user/gopher", nil)

	w := new(mockResponseWriter)
	ps := httprouter.Params{}

	requestHandler1(w, r, ps)

	testCounter(t, counter1, 1, "mid1")
	testCounter(t, mid2, 1, "mid2")
	testCounter(t, counter3, 1, "mid3")
	testCounter(t, mid4, 0, "mid4")

}

func TestManager_With(t *testing.T) {
	manager := New()
	mid1, counter1 := newMiddlewareFunc("")
	manager.UseHandler(mid1)

	mid2, counter2 := newMiddlewareFunc("")

	subOne := manager.With(mid2)

	mid3, counter3 := newMiddlewareFunc("")

	// subTwo
	_ = manager.With(mid3)

	mid4, counter4 := newMiddlewareFunc("")

	// subThree
	_ = subOne.With(mid4)

	r, _ := http.NewRequest("GET", "/user/gopher", nil)

	w := new(mockResponseWriter)
	ps := httprouter.Params{}

	subOne.GetHandle()(w, r, ps)

	testCounter(t, counter1, 1, "mid1")
	testCounter(t, counter2, 1, "mid2")
	testCounter(t, counter3, 0, "mid3")
	testCounter(t, counter4, 0, "mid4")

}

func TestTabiBackendCase(t *testing.T) {
	midRoot := New()
	mid1, counter1 := newMiddlewareFunc("mid1")
	mid2, counter2 := newMiddlewareFunc("mid2")
	mid3, counter3 := newMiddlewareFunc("mid3")
	mid4, counter4 := newMiddlewareFunc("mid4")
	mid5, counter5 := newMiddlewareFunc("mid5")
	mid6, counter6 := newMiddlewareFunc("mid6")
	mid7, counter7 := newMiddlewareFunc("mid7")
	//mid11, counter11 := newMiddlewareFunc("mid11")

	midRoot.UseHandler(mid1)
	midRoot.UseHandler(mid2)
	midRoot.UseHandler(mid3)
	midRoot.UseHandler(mid4)
	midRoot.UseHandler(mid5)
	midRoot.UseHandler(mid6)
	midRoot.UseHandler(mid7)
	//midRoot.UseHandler(mid11)

	midSubOne := *midRoot

	mid8, counter8 := newMiddlewareFunc("mid8")
	midSubOne.UseHandler(mid8)

	mid9, counter9 := newMiddlewareFunc("mid9")
	midSubTwo := midRoot.With(mid9)

	mid10, counter10 := newMiddlewareFunc("mid10")
	midSubThree := midRoot.With(mid10)

	handler1 := createNewHandler("Result from handler 1")
	handler2 := createNewHandler("Result from handler 2")
	handler3 := createNewHandler("Result from handler 3")

	router := httprouter.New()

	var chosen bool

	router.GET("/api/v1/ping", midSubOne.UseHandleAndServe(handler1))
	router.POST("/api/v1/user/:uid/device", checkRightRouteChosen(midSubTwo.UseHandleAndServe(handler2), &chosen))
	router.GET("/api/v1/user/:uid/device", midSubThree.UseHandleAndServe(handler3))

	r, _ := http.NewRequest("POST", "/api/v1/user/2/device", nil)
	w := httptest.NewRecorder()

	router.ServeHTTP(w, r)

	if !chosen {
		t.Error("Unexpected route was chosen by httprouter")
	}

	// First seven middleware are part of midRoot and should always be called
	testCounter(t, counter1, 1, "mid1")
	testCounter(t, counter2, 1, "mid2")
	testCounter(t, counter3, 1, "mid3")
	testCounter(t, counter4, 1, "mid4")
	testCounter(t, counter5, 1, "mid5")
	testCounter(t, counter6, 1, "mid6")
	testCounter(t, counter7, 1, "mid7")
	//testCounter(t, counter11, 1, "mid11")

	// mid8 is part of midSubOne and should not be called
	testCounter(t, counter8, 0, "mid8")
	// mid9 is part of midSubTwo and should not be called
	testCounter(t, counter9, 1, "mid9")
	// mid10 is part of unused midSubThree and should not be called
	testCounter(t, counter10, 0, "mid10")

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	bodyStr := string(body)

	if bodyStr != "Result from handler 2" {
		t.Error("Did not receive from handler 2")
	}

}

func TestTabiBackendStructBasedCase(t *testing.T) {
	midRoot := New()
	mid1 := newMiddlewareStruct("mid1")
	mid2 := newMiddlewareStruct("mid2")
	mid3 := newMiddlewareStruct("mid3")
	mid4 := newMiddlewareStruct("mid4")
	mid5 := newMiddlewareStruct("mid5")
	mid6 := newMiddlewareStruct("mid6")
	mid7 := newMiddlewareStruct("mid7")
	//mid11, counter11 := newMiddlewareFunc("mid11")

	midRoot.UseHandler(mid1)
	midRoot.UseHandler(mid2)
	midRoot.UseHandler(mid3)
	midRoot.UseHandler(mid4)
	midRoot.UseHandler(mid5)
	midRoot.UseHandler(mid6)
	midRoot.UseHandler(mid7)
	//midRoot.UseHandler(mid11)

	mid8 := newMiddlewareStruct("mid8")
	midSubOne := midRoot.With(mid8)
	fmt.Println(midSubOne)

	mid9 := newMiddlewareStruct("mid9")
	midSubTwo := *midRoot.With(mid9)
	fmt.Println(midSubTwo)

	mid10 := newMiddlewareStruct("mid10")
	midSubThree := *midRoot.With(mid10)
	fmt.Println(midSubThree)

	handler1 := createNewHandler("Result from handler 1")
	handler2 := createNewHandler("Result from handler 2")
	handler3 := createNewHandler("Result from handler 3")

	router := httprouter.New()

	var chosen bool

	router.GET("/api/v1/ping", midSubOne.UseHandleAndServe(handler1))
	router.POST("/api/v1/user/:uid/device", checkRightRouteChosen(midSubTwo.UseHandleAndServe(handler2), &chosen))
	router.GET("/api/v1/user/:uid/device", midSubThree.UseHandleAndServe(handler3))

	r, _ := http.NewRequest("POST", "/api/v1/user/2/device", nil)
	w := httptest.NewRecorder()

	router.ServeHTTP(w, r)

	if !chosen {
		t.Error("Unexpected route was chosen by httprouter")
	}

	// First seven middleware are part of midRoot and should always be called
	testCounter(t, mid1, 1, "mid1")
	testCounter(t, mid2, 1, "mid2")
	testCounter(t, mid3, 1, "mid3")
	testCounter(t, mid4, 1, "mid4")
	testCounter(t, mid5, 1, "mid5")
	testCounter(t, mid6, 1, "mid6")
	testCounter(t, mid7, 1, "mid7")
	//testCounter(t, counter11, 1, "mid11")

	// mid8 is part of midSubOne and should not be called
	testCounter(t, mid8, 0, "mid8")
	// mid9 is part of midSubTwo and should not be called
	testCounter(t, mid9, 1, "mid9")
	// mid10 is part of unused midSubThree and should not be called
	testCounter(t, mid10, 0, "mid10")

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	bodyStr := string(body)

	if bodyStr != "Result from handler 2" {
		t.Error("Did not receive from handler 2")
	}

}

func TestHandlerVerification(t *testing.T) {
	n := New()

	mid1 := newMiddlewareStruct("mid1")
	mid2 := newMiddlewareStruct("mid2")
	mid3 := newMiddlewareStruct("mid3")
	mid4 := newMiddlewareStruct("mid4")
	mid5 := newMiddlewareStruct("mid5")
	mid6 := newMiddlewareStruct("mid6")
	mid7 := newMiddlewareStruct("mid7")

	n.UseHandler(mid1)
	n.UseHandler(mid2)
	n.UseHandler(mid3)
	n.UseHandler(mid4)
	n.UseHandler(mid5)
	n.UseHandler(mid6)
	n.UseHandler(mid7)

	mid8 := newMiddlewareStruct("mid8")
	subNeg1 := n.With(mid8)

	mid9 := newMiddlewareStruct("mid9")
	subNeg2 := n.With(mid9)

	mid10 := newMiddlewareStruct("mid10")
	subNeg3 := n.With(mid10)

	fmt.Println(subNeg1)
	fmt.Println(subNeg3)

	if !sameHandlers(subNeg1.handlers, []Handler{mid1, mid2, mid3, mid4, mid5, mid6, mid7, mid8}) {
		t.Error("Handlers not the same")
	}
	if !sameHandlers(subNeg2.handlers, []Handler{mid1, mid2, mid3, mid4, mid5, mid6, mid7, mid9}) {
		t.Error("Handlers not the same")
	}
	if !sameHandlers(subNeg3.handlers, []Handler{mid1, mid2, mid3, mid4, mid5, mid6, mid7, mid10}) {
		t.Error("Handlers not the same")
	}

	response := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/some_url", nil)
	subNeg2.GetHandle()(response, r, httprouter.Params{})

	testCounter(t, mid1, 1, "mid1")
	testCounter(t, mid2, 1, "mid2")
	testCounter(t, mid3, 1, "mid3")
	testCounter(t, mid4, 1, "mid4")
	testCounter(t, mid5, 1, "mid5")
	testCounter(t, mid6, 1, "mid6")
	testCounter(t, mid7, 1, "mid7")

	// mid8 is part of midSubOne and should not be called
	testCounter(t, mid8, 0, "mid8")
	// mid9 is part of midSubTwo and should not be called
	testCounter(t, mid9, 1, "mid9")
	// mid10 is part of unused midSubThree and should not be called
	testCounter(t, mid10, 0, "mid10")

}

func sameHandlers(handlers []Handler, handlers2 []Handler) bool {
	for i, v := range handlers {
		sf1 := reflect.ValueOf(v)
		sf2 := reflect.ValueOf(handlers2[i])

		if sf1 != sf2 {
			return false
		}
	}
	return true
}

func testCounter(t *testing.T, counter *MockMiddleware, expected int, identifier string) {
	if counter.Counter != expected {
		t.Errorf("Expected %d call to middleware instead %d (%s)", expected, counter.Counter, identifier)
	}
}

func checkRightRouteChosen(handle httprouter.Handle, successOutput *bool) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		*successOutput = true
		handle(w, r, ps)
	}

}
